My name is Amy Orlando and I was born and raised in a suburb outside of Milwaukee Wisconsin. I moved to the west coast for college where I majored in both Psychology and Sociology. I now work in at a marketing firm in Los Angeles. I have always been very interested in people and human society which my schooling allowed me to understand them from a scientific perspective. People and the interactions between them are prevalent in all facets of our lives especially in the area of dating and relationships. So when I was not having great success meeting the right guys with the traditional avenues of bars and friends of friends, I thought to myself there must be a better approach. This is when I discovered the world of online dating! I quickly discovered that just like with any other form of dating, online dating is not perfect and you need to use the right tools and strategies to be successful. 

Because of this, I decided to create this website with the help of others, both male and female, to provide helpful guide to online dating for all so you too can have the success myself and many others have found with finding that special someone online.

Address: 300 East 23rd Street, Apt 10E, New York, NY 10010

Phone: 424-345-0340
